#include <my/cppstring.h>
#include <gtest/gtest.h>


TEST(string, size)
{
    String s0;
    String s(3, 'a');
    String s1(s);
    EXPECT_EQ(s.Size(), 3u);
    EXPECT_EQ(s1.Size(), 3u);
    EXPECT_EQ(s0.Size(), 0u);
}

TEST(string, test)
{
    String test1("test1");
    String test2("test2");
    EXPECT_EQ("test1test2", test1 + test2);
}


int main(int argc, char* argv[]){
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
