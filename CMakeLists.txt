cmake_minimum_required(VERSION 3.0)
project(mystring)

add_library(${PROJECT_NAME} my/cppstring.cpp)
target_include_directories(${PROJECT_NAME} PUBLIC .)
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_20)
